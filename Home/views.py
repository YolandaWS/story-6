from django.shortcuts import render
from .forms import MyForm
from .models import AddStatus
from django.http import HttpResponseRedirect

# Create your views here.
def index(request):
    YourStatus =AddStatus.objects.all().order_by('-date')
    if request.method=="POST":
        form=MyForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/')

    else:
        form=MyForm()
    
    return render(request,'Story6.html',{'form':form, 'YourStatus':YourStatus})
    