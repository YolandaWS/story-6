from django.db import models

class AddStatus(models.Model):
    status = models.CharField(max_length=150)
    date=models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return '%s' % self.status
